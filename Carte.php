<?php

class Carte
{
    private $libelle;

    private $valeur;

    private $signe;

    public function __construct($libelle, $valeur, $signe)
    {
        $this->libelle = $libelle;
        $this->valeur = $valeur;
        $this->signe = $signe;
    }

    /**
     * Get the value of valeur
     */ 
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * Set the value of valeur
     *
     * @return  self
     */ 
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;

        return $this;
    }

    /**
     * Get the value of signe
     */ 
    public function getsigne()
    {
        return $this->signe;
    }

    /**
     * Set the value of signe
     *
     * @return  self
     */ 
    public function setsigne($signe)
    {
        $this->signe = $signe;

        return $this;
    }

    

    /**
     * Get the value of libelle
     */ 
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set the value of libelle
     *
     * @return  self
     */ 
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function afficheCarte()
    {
        $message = "La carte est le : " . $this->getLibelle();
        return $message;
    }
}

?>
