<?php
require_once 'User.php';
require_once 'Carte.php';


// Cartes hautes
$tabCarteHaute = ['valet', 'dame', 'roi', 'as'];

// Cartes
$paquets = [];

// Création Cartes Basses
for ($i=2; $i < 11; $i++) { 
    // Carte coeur
    $paquets[] = new Carte("$i de coeur", $i, "coeur");
    // Carte pique
    $paquets[] = new Carte("$i de pique", $i, "pique");
    // Carte carreau
    $paquets[] = new Carte("$i de carreau", $i, "carreau");
    // Carte trefle
    $paquets[] = new Carte("$i de trefle", $i, "trefle");
}

// Création Cartes Hautes
$i = 11;
foreach ($tabCarteHaute as $carteHaute) {
    // Carte coeur
    $paquets[] = new Carte("$carteHaute de coeur", $i, "coeur");
    // Carte pique
    $paquets[] = new Carte("$carteHaute de pique", $i, "pique");
    // Carte carreau
    $paquets[] = new Carte("$carteHaute de carreau", $i, "carreau");
    // Carte trefle
    $paquets[] = new Carte("$carteHaute de trefle", $i, "trefle");
    $i++;
}


shuffle($paquets);

$paquetMat = [];
$paquetSoso = [];

foreach($paquets as $carte){
    $i = 0;
    if($i % 2 == 0 && count($paquetMat) < 26){
        $paquetMat[] = $carte;
    }else{
        $paquetSoso[] = $carte;
    }
    $i++;
}

// Création des User
$soso = new User("soso", $paquetSoso);
$mathis = new User("matmat", $paquetMat);

echo "Appuyez sur entrée pour commencer la partie !";

$tasCartes = [];


$pointsSoso = 0;
$pointsMat = 0;

// ERREUR SUR LE FOR A CORRIGER AU PLUS VITE

//for($i = 0; $i < 26;$i++){
while($paquetSoso[0] != null || $paquetMat[0] != null){
//    readline();

    $carteSoso = $paquetSoso[0]->getValeur();
    $carteMat = $paquetMat[0]->getValeur();


    echo "la carte de Soso est : " . $paquetSoso[0]->getLibelle() . "\n";
    echo "la carte de Mat est : " . $paquetMat[0]->getLibelle() . "\n";

//    On met dans le tasCartes les deux cartes
    array_push($tasCartes, array_shift($paquetSoso));
    array_push($tasCartes, array_shift($paquetMat));

    if($carteSoso > $carteMat){
        echo "Le gagnant du tour est Soso" . "\n";
        array_push($paquetSoso, ...$tasCartes);
        $tasCartes = [];
        $pointsSoso++;
    }elseif ($carteSoso < $carteMat){
        echo "Le gagnant du tour est Mat" . "\n";
        array_push($paquetMat, ...$tasCartes);
        $tasCartes = [];
        $pointsMat++;
    }elseif ($carteSoso == $carteMat){
        echo "Égalité mais rejouons !" . "\n";
    }


    echo "Points de Soso : $pointsSoso" . "\n";
    echo "Points de Mat : $pointsMat" . "\n";

}

echo "Nb cartes paquet soso : " . count($paquetSoso) . "\n";
echo "Nb cartes paquet mat : " . count($paquetMat) . "\n";

$msg = $pointsSoso > $pointsMat ? 'Le grand gagnant est Soso' : 'Le grand gagnant est Mat ' . "\n";
echo $msg;

var_dump( count($paquetSoso), count($paquetMat));
die();

?>
