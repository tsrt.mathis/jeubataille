<?php

class User
{
    private $username;
    private $paquet;

    public function __construct($username, $paquet)
    {
        $this->username = $username;
        $this->paquet = $paquet;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPaquet()
    {
        return $this->paquet;
    }

    public function setPaquet($paquet)
    {
        $this->paquet = $paquet;
    }

    public function afficheUsername()
    {
        $message = "Le username du user est : " . $this->getUsername();
        return $message;
    }
}

?>
